import React from 'react';
import { render } from 'react-dom';
import TodoItem from './TodoItem';
import { Input } from 'material-ui';

export default class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      todoItemList: [
        <TodoItem key="1" id="1" title="Do the dishes" deleteItem={this.deleteItem.bind(this)} />,
        <TodoItem key="2" id="2" title="Walk the dog" deleteItem={this.deleteItem.bind(this)} />
      ]
    };
  }

  addTodoItem(e) {
    e.preventDefault();

    if (this.state.inputValue !== '') {
      //id cannot start with a number, su add a letter at the begining 
      let newId = 'tdi-' + Math.random().toString(36).substring(2, 15);
      this.setState({
        todoItemList: [
          ...this.state.todoItemList,
          <TodoItem
            key={newId}
            id={newId}
            title={this.state.inputValue}
            deleteItem={this.deleteItem.bind(this)}
          />
        ]
      });
      this.setState({
        inputValue: '',
      })
    }
  }

  deleteItem(itemId) {
    let newList = this.state.todoItemList.filter(item => item.props.id !== itemId);
    this.setState({ todoItemList: newList });
  }

  inputChanged(e) {
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    const { todoItemList } = this.state;

    return <div>
      <div className="title">{this.props.header}</div>
      <form onSubmit={this.addTodoItem.bind(this)}>
        <Input value={this.state.inputValue}
          className="input"
          type="text"
          onChange={this.inputChanged.bind(this)} 
          value={this.state.inputValue} 
          placeholder="What needs to be done?"/>
      </form>
      <div>
        <ul>
          {this.state.todoItemList}
        </ul>
      </div>
    </div>
  }
}
