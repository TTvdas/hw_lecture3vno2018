import React from 'react';
import { Checkbox } from 'material-ui'
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';

class TodoItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      checkboxChecked: false,
      className: "todo-item",
    }
  }

  toggleChange() {
    this.setState({
      checkboxChecked: !this.state.checkboxChecked,
      className: this.state.checkboxChecked ? "todo-item" : "todo-item line-through",
    })
  }

  render() {
    return (
      <li className={this.state.className}>
        {this.props.title}
        {
          !this.state.checkboxChecked &&
          <IconButton className="icon" color="primary" aria-label="Delete" onClick={() => this.props.deleteItem(this.props.id)}>
            <DeleteIcon />
          </IconButton>
        }
        <Checkbox className="icon" checked={this.state.isChecked} onChange={this.toggleChange.bind(this)} />
      </li>
    )
  }
}

export default TodoItem;